package com.gestionevenements.evenementsservingwebcontent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvenementsservingwebcontentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvenementsservingwebcontentApplication.class, args);
	}

}
